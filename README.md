# Gitlab CI helpers

Scripts to make CI usage easier. Just source them in your current CI script like

```
. <(curl -sL https://gitlab.com/morph027/gitlab-ci-helpers/raw/master/check-vars.sh)
```

## More snippets

Just check the [Wiki](https://gitlab.com/morph027/gitlab-ci-helpers/wikis/home) for more useful stuff

## Helpers

### check-vars.sh

Checks for a predefined set of variables in bash array ```VAR=()```.

Functions:

* ```check_vars```

### swarm.sh

Docker Swarm Helpers.

Required ENV variables:

* ```DOCKER_HOST```: connection to Docker Swarm manager node

Functions:

* ```swarm_service_exists $SERVICE```: check, if service defined as first parameter exists in Swarm
* ```swarm_get_node $SERVICE```: get the current running node of a scale=1 service
* ```swarm_restart_service $SERVICE```: restarts the service
* ```swarm_refresh_image $IMAGE $SERVICE```: pulls the (probably updated like ```latest```) image again restarts the service.
* ```swarm_deploy_service```: deploy a new service, ```$@``` will be passed to ```docker service create```

### get-last-successful-build-artifact.sh

Grabs the last successful build artifact. You'll need the project id instead of the name, which you can either read from the trigger settings page or from the api like:

```
curl -s -H "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "${BASE_URL}/api/v4/projects?per_page=1000" | jq '.[] | select(.name=="${PROJECT_NAME}")'
```

Required ENV variables:

* ```BASE_URL```: The base url of your gitlab instance (e.g. ```https://gitlab.com```), defaults to ```echo $CI_PROJECT_URL |  cut -d'/' -f1-3``` if unset
* ```PRIVATE_TOKEN```
* ```PROJECT```
* ```STAGE```: define stage, from which artifact gets pulled
* ```PER_PAGE```: define how many results get queried from the API, defaults to 50

Optional ENV variables:

* ```OUT_FILE```: defines, where the download will be stored (e.g. _/some/place/this.zip_). Defaults to _$(pwd)/artifacts.zip_
* ```REF```: defines the commit revision (like ```CI_BUILD_REF```), from which the last build gets downloaded. Defaults to _master_
* ```COMMIT```: defines the short_id of a commit revision (like first 8 chars of```CI_BUILD_REF```), from which the last build gets downloaded.

Functions:

* ```download_latest```

### docker-build.sh

Build a docker image from _Dockerfile_ and push to registry.

Required ENV variables:

* ```IMAGE_NAME```
* ```REGISTRY```
* ```NAMESPACE```

Optional ENV variables:

* ```DOCKER_TAG```: defines tag to use when tagging the image. If emtpy, ```${CI_BUILD_TAG}``` will be used.
* ```KEEP_LOCAL_IMAGES```: after uploading the image to the registry, do not remove local images (default: delete).

Functions:

* ```docker_build```
* ```docker_tag```
* ```docker_push```

### keep-tagged-artifacts.sh

The CI job will match ```KEEP_BUILD_TAG``` with ```KEEP_TAG_REGEX``` to decide if it keep it's own artifacts, when they are set for expiration by default (e.g. for releases,...).

We need to track the ```CI_BUILD_ID``` of the job, we possibly want to keep via artifacts.

Create (add to ```script:``` section of the job):

```
  - mkdir -p .gitlab-ci/ci-build-ids/ && echo $CI_BUILD_ID > .gitlab-ci/ci-build-ids/$CI_BUILD_ID
```

Archive (add to ```paths:``` inside ```artifacts:``` section of the job):

```
    - .gitlab-ci/ci-build-ids/$CI_BUILD_ID
```

#### Example

* have a look at the inline comments inside jobs you want to protect
* the whole ```keep-artifacts``` stage does the job

``` yaml
variables:
  KEEP_TAG_REGEX: "foo.*|bar.*"

stages:
  - foo
  - bar
  - keep-artifacts

foo:
  stage: foo
  script:
  - make foo
  - mkdir -p .gitlab-ci/ci-build-ids/ && echo $CI_BUILD_ID > .gitlab-ci/ci-build-ids/$CI_BUILD_ID # <<< this will record the current CI_BUILD_ID
  artifacts:
    paths:
    - foo
    - .gitlab-ci/ci-build-ids/$CI_BUILD_ID # <<< this will archive the current CI_BUILD_ID
    expire_in: 2 weeks

bar:
  stage: bar
  script:
  - make foo
  - mkdir -p .gitlab-ci/ci-build-ids/ && echo $CI_BUILD_ID > .gitlab-ci/ci-build-ids/$CI_BUILD_ID  # <<< this will record the current CI_BUILD_ID
  artifacts:
    paths:
    - bar
    - .gitlab-ci/ci-build-ids/$CI_BUILD_ID # <<< this will archive the current CI_BUILD_ID
    expire_in: 2 weeks

keep-artifacts:
  stage: keep-artifacts
  script:
  - for KEEP_BUILD_ID in $(ls .gitlab-ci/ci-build-ids/); do . <(curl -fksSL https://gitlab.hq.packetwerk.com/gitlab/ci-helpers/raw/master/keep-tagged-artifacts.sh); done
```
